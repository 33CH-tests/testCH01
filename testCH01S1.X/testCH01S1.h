/*! \file  testCH01S1.h
 *
 *  \brief Constants and function prototypes for testCH01S1
 *
 *  \author jjmcd
 *  \date July 12, 2018, 10:50 AM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef TESTCH01S1_H
#define	TESTCH01S1_H

#ifdef	__cplusplus
extern "C"
{
#endif

  void initOscillator(void);

#ifdef	__cplusplus
}
#endif

#endif	/* TESTCH01S1_H */

