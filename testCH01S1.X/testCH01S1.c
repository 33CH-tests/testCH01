/*! \file  testCH01S1.c
 *
 *  \brief This file contains the mainline for testCH01S1 - slave core
 *
 *
 *  \author jjmcd
 *  \date July 11, 2018, 7:01 PM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#include <xc.h>
#include "testCH01S1.h"

#define LED_ORANGE _LATB5
#define SNORE_COUNT 500000

/*! snore - waste some time */
void snore( long j )
{
  long i;
  for ( i=0; i<j; i++ )
    ;
}

/*! main - Mainline for testCH01-S */
/*! Simply flashes the orange LED rather quickly
 */
int main(void)
{
  initOscillator();

  _TRISB5 = 0;

  while (1)
    {
      while( !IFS8bits.MSIAIF )
        ;
      if ( SI1MBX0D )
        LED_ORANGE=1;
      else
        LED_ORANGE=0;
    }
  return 0;
}
