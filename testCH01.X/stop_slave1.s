;! \file  stop_slave1.c
;
;  \brief Stop the slave core by clearing the SLVEN bit
;
;  \author jjmcd
;  \date July 14, 2018, 8:47 AM
;
; Software License Agreement
; Copyright (c) 2018 by John J. McDonough, WB8RCR
; This software is released under the GNU General Public License V2.
; See the included files for a complete description.
;
	    .include	"p33CH128MP505.inc"

;! stop_slave1 - Stop the slave core by clearing the SLVEN bit
;! Unlocks the MSI1 by sending the MSI key sequence, then
;  disables the slave by clearing the SLVEN bit.
;
	    .global	_stop_slave1
_stop_slave1:
	    lnk		#0x0
	    ; MSI1 unlock sequence
	    mov.b	#0x55,W0
	    mov.b	WREG,MSI1KEY
	    mov.b	#0xaa,W0
	    mov.b	WREG,MSI1KEY
	    ; SLVEN bit is in high byte of MSI1CON
	    ; Generated code is bclr MSI1CONH,#7 (SLVEN is 0xF)
	    bclr	MSI1CON,#SLVEN
	    ulnk
	    return



