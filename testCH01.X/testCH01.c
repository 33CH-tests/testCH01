/*! \file  testCH01.c
 *
 *  \brief This file contains the mainline for testCH01 - master core
 *
 *
 *  \author jjmcd
 *  \date July 10, 2018, 2:58 PM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#include <xc.h>
#include <libpic30.h>
#include "testCH01.h"
#include "testCH01S1.h"

#define LED_BLUE   _LATC8
#define LED_PINK   _LATC9
#define LED_ORANGE _LATB5

#define COUNT 5000000           /* Slowest rate to flash the LEDs */
#define MINCOUNT 100000         /* Fastest rate to flash the LEDs */
#define DELAY_DECREMENT 100000  /* Decrement for flashing rate */

/*! snore - waste some time */
void snore(long lDelayCount)
{
  long i;
  for (i = 0; i < lDelayCount; i++)
    ;
}

/*! main - Mainline for testCH01 - master core */
/*! Load and start the slave core program. Then begin alternately
 *  flashing the blue and pink LEDs at a slowly increasing rate.
 */
int main(void)
{
  long lDelayCount;
  int nSave;

  /* Disable Watch Dog Timer */
  RCONbits.SWDTEN = 0;

  _TRISC8 = 0;  /* Blue LED */
  _TRISC9 = 0;  /* Pink LED */

  /* Program and start the slave */
  _program_slave(1, 0, testCH01S1);
  _start_slave();

  /* Initialize the oscillator */
  initOscillator();

  /* Blink time will decrease slowly */
  lDelayCount = COUNT;

  nSave = 0;
  while (1)
    {
      snore(lDelayCount);
      LED_BLUE ^= 1;
      snore(lDelayCount);
      LED_PINK ^= 1;
      lDelayCount -= DELAY_DECREMENT;
      if (lDelayCount < MINCOUNT)
        lDelayCount = COUNT;

      /* Whenever the LEDs are both lit, toggle the run state of the slave */
      if ( LED_PINK && LED_BLUE )
        nSave ^= 1;
      if ( nSave )
        sendSlave(1);
      else
        sendSlave(0);
    }

  return 0;
}
