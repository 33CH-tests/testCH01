/*! \file  testCH01.h
 *
 *  \brief Constants and function prototypes for testCH01
 *
 *  \author jjmcd
 *  \date July 12, 2018, 10:52 AM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef TESTCH01_H
#define	TESTCH01_H

#ifdef	__cplusplus
extern "C"
{
#endif

void initOscillator( void );
void stop_slave1(void);
void setupMailbox(void);
void sendSlave(int);

#ifdef	__cplusplus
}
#endif

#endif	/* TESTCH01_H */

